import { GitlabNotifyPage } from './app.po';

describe('gitlab-notify App', () => {
  let page: GitlabNotifyPage;

  beforeEach(() => {
    page = new GitlabNotifyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
