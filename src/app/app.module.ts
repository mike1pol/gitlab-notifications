/** System imports */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { LocalStorageModule, ILocalStorageServiceConfig } from 'angular-2-local-storage';
import { MomentModule } from 'angular2-moment';
import 'hammerjs';
/** Service imports */
import { SettingsService } from './service/settings.service';
import { ClientService } from './service/client.service';
/** Component imports */
import { AppComponent } from './app.component';
import { SettingsComponent } from './settings/settings.component';
import { IndexComponent } from './index/index.component';
/** Route list */
import { RouteList } from './routes';
import { TodoComponent } from './todo/todo.component';
/** Local storage settings */
const localStorage: ILocalStorageServiceConfig = {
  prefix: 'gitlab',
  storageType: 'localStorage'
};
@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    IndexComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(RouteList, { useHash: true }),
    MaterialModule.forRoot(),
    LocalStorageModule.withConfig(localStorage),
    MomentModule
  ],
  providers: [
    SettingsService,
    ClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
