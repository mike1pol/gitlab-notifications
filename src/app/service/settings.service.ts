import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { Settings } from '../interface/settings';

@Injectable()
export class SettingsService {

  private settingsName = 'settings';
  private loadingName = 'loading';

  constructor(
    private localStorage: LocalStorageService
  ) {
  }

  get settings(): Settings {
    let settings = this.localStorage.get<Settings>(this.settingsName);
    if (!settings) {
      settings = {
        hostname: 'https://gitlab.com',
        token: null
      };
      this.setSettings(settings);
    }
    return settings;
  }

  setSettings(settings: Settings): boolean {
    return this.localStorage.set(this.settingsName, settings);
  }
}
