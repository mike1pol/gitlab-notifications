import { Injectable } from '@angular/core';
import { Http, Headers, Response, Request, RequestOptions } from '@angular/http';
import { LocalStorageService } from 'angular-2-local-storage';

import { SettingsService } from './settings.service';

import { TodoResult } from '../interface/todo';

@Injectable()
export class ClientService {

  constructor(
    private http: Http,
    private localStorage: LocalStorageService,
    private settingsService: SettingsService
  ) { }

  client(method: string, url: string, body?: any): Promise<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('PRIVATE-TOKEN', this.settingsService.settings.token);
    const options = new RequestOptions({
      method,
      headers,
      url: `${this.settingsService.settings.hostname}${url}`,
      body
    });
    const request: Request = new Request(options);
    return new Promise((resolve, reject) => {
      this.http
        .request(request)
        .subscribe((res: Response) => {
          if (method === 'head') {
            const obj = {};
            res.headers.forEach((v, n) => {
              obj[n] = v;
            });
            return resolve(obj);
          }
          const data = res.json();
          resolve(data);
        },
        (err) => {
          try {
            err = err.json();
          } catch (e) {
            console.error(e);
          }
          reject(err);
        });
    });
  }

  getTodos(page: number = 1, pending?: boolean): Promise<TodoResult> {
    const per_page = 20;
    const res: TodoResult = {
      pages: {
        total: 0,
        pages: 0
      },
      todos: null
    };
    return this.client('head', `/api/v3/todos?state=${pending ? 'pending' : 'done'}&page=${page}&per_page=${per_page}`)
      .then((dd) => {
         res.pages.total = dd['X-Total'][0];
         res.pages.pages = dd['X-Total-Pages'][0];
        return this.client('get', `/api/v3/todos?state=${pending ? 'pending' : 'done'}&page=${page}&per_page=${per_page}`);
      })
      .then((todos) => {
        res.todos = todos;
        return res;
      });
  }

  doneTodo(id: number): Promise<void> {
    return this.client('delete', `/api/v3/todos/${id}`);
  }

}
