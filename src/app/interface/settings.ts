/**
 * Settings
 * @export
 */
export interface Settings {
  /** Hostname */
  hostname: string;
  /** API token */
  token: string;
}
