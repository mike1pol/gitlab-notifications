export interface Paginator {
  total: number;
  pages: number;
};

export interface TodoResult {
  pages: Paginator;
  todos: Todo.RootObject[];
};

export module Todo {

  export interface Project {
    id: number;
    http_url_to_repo: string;
    web_url: string;
    name: string;
    name_with_namespace: string;
    path: string;
    path_with_namespace: string;
  }

  export interface User {
    id: number;
    username: string;
    name: string;
    state: string;
    avatar_url: string;
    web_url: string;
  }

  export interface Target {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: Date;
    updated_at: Date;
    target_branch: string;
    source_branch: string;
    upvotes: number;
    downvotes: number;
    author: User;
    assignee: User;
    source_project_id: number;
    target_project_id: number;
    labels: any[];
    work_in_progress: boolean;
    milestone?: any;
    merge_when_build_succeeds: boolean;
    merge_status: string;
    sha: string;
    merge_commit_sha?: any;
    subscribed: boolean;
    user_notes_count: number;
    approvals_before_merge?: any;
    should_remove_source_branch?: any;
    force_remove_source_branch: boolean;
    squash: boolean;
    web_url: string;
  }

  export interface RootObject {
    id: number;
    project: Project;
    author: User;
    action_name: string;
    target_type: string;
    target: Target;
    target_url: string;
    body: string;
    state: string;
    created_at: Date;
  }

}
