import { Component } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { SettingsService } from '../service/settings.service';
import { Settings } from '../interface/settings';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent {

  public settings: Settings;
  private settingsSaveType = SettingsSaveType;

  constructor(
    private settingsService: SettingsService,
    private snackBar: MdSnackBar
  ) {
    this.settings = this.settingsService.settings;
  }

  update($event, type: SettingsSaveType) {
    if (type === this.settingsSaveType.Host) {
      this.settings.hostname = $event.target.value;
    } else if (type === this.settingsSaveType.Token) {
      this.settings.token = $event.target.value;
    }
    this.snackBar.open('Settings saved', 'OK', {
      duration: 3000,
    });
    this.settingsService.setSettings(this.settings);
  }
}


enum SettingsSaveType {
  Host,
  Token
}
