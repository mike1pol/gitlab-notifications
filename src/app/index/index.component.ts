import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

const { ipcRenderer } = electron;

import { Settings } from '../interface/settings';
import { SettingsService } from '../service/settings.service';
import { ClientService } from '../service/client.service';

import { Todo } from '../interface/todo';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnDestroy {
  private activeTab = 0;

  public active: Todo.RootObject[] = [];
  public activeTotal: number = null;
  public activeLoading: boolean;

  public done: Todo.RootObject[] = [];
  public doneTotal: number = null;
  public doneLoading: boolean;

  private timeout;

  constructor(
    private settingsService: SettingsService,
    private clientService: ClientService,
    private router: Router,
    private snackBar: MdSnackBar
  ) {
    const settings: Settings = this.settingsService.settings;
    if (!settings.token) {
      this.router.navigate(['settings']);
    } else {
      this.getTodos();
    }
  }

  ngOnDestroy() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
  }

  get loading(): boolean {
    if (this.activeTab === 0) {
      return this.activeLoading;
    }
    return this.doneLoading;
  }

  update() {
    if (this.activeTab === 0) {
      this.getAcitve();
    } else {
      this.getDone();
    }
  }

  tabChanged($event) {
    this.activeTab = $event.index;
  }

  private getAcitve(page = 1): Promise<any> {
    this.activeLoading = true;
    return this.clientService.getTodos(page, true)
      .then((data) => {
        this.activeTotal = data.pages.total;
        this.active = data.todos;
        this.activeLoading = false;
        this.updateIcon();
      });
  }

  private getDone(page = 1): Promise<any> {
    this.doneLoading = true;
    return this.clientService.getTodos(page)
      .then((data) => {
        this.doneTotal = data.pages.total;
        this.done = data.todos;
        this.doneLoading = false;
      });
  }


  private getTodos() {
    Promise.all([this.getAcitve(), this.getDone()])
      .then(() => {
        // this.timeout = setTimeout(() => {
        //   this.getTodos();
        // }, 1000 * 60);
      })
      .catch(console.error);
  }

  updateIcon() {
    if (this.active && this.active.length > 0) {
      ipcRenderer.send('update-icon', 'TrayActive');
    } else {
      ipcRenderer.send('update-icon', 'TrayIdle');
    }
  }

  doneTodo(id: number) {
    const el = this.active.find(v => v.id !== id);
    this.active = this.active.filter(v => v.id !== id);
    this.clientService.doneTodo(id)
      .then(() => {
        this.done.unshift(el);
        this.updateIcon();
      })
      .catch((err) => {
        this.snackBar.open('Error change state', 'OK', {
          duration: 3000,
        });
        this.active.push(el);
      });
  }

}
