/** System imports */
import { Component } from '@angular/core';
/**
 * App root component
 * @export
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * Constructor of app component
   */
  constructor(
  ) {
  }
}
