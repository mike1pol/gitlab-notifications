/** System imports */
import { Route } from '@angular/router';

/** Component imports  */
import { IndexComponent } from './index/index.component';
import { SettingsComponent } from './settings/settings.component';

/** Route list */
export const RouteList: Route[] = [
  /** Index page */
  {
    path: 'index',
    component: IndexComponent
  },
  /** Settings page */
  {
    path: 'settings',
    component: SettingsComponent
  },
  /** Redirect all undefined to index */
  {
    path: '**',
    redirectTo: 'index'
  }
];
