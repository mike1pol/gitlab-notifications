import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../interface/todo';

const { ipcRenderer, shell } = electron;

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @Input('todo') todo: Todo.RootObject;
  @Output('remove') remove = new EventEmitter();

  constructor() {
  }

  openUrl($event) {
    $event.preventDefault();
    shell.openExternal($event.target.href);
  }

  getAction(action: string): string {
    if (action === 'assigned') {
      return 'assigned you';
    } else if (action === 'approval_required') {
      return 'set you as an approver for';
    }
    return action;
  }

  getTargetType(type: string): string {
    if (type === 'MergeRequest') {
      return 'merge request';
    }
    return 'issue';
  }

  doneTodo(id: number) {
    this.remove.emit(id);
  }
}
